
Fonte: [dadosabertos.capes.gov.br](https://dadosabertos.capes.gov.br/dataset?groups=avaliacao-da-pos-graduacao&organization=diretoria-de-avaliacao)

Os dados de 2017 e 2018 se originam [desse dataset](https://dadosabertos.capes.gov.br/dataset/2017-2020-discentes-da-pos-graduacao-stricto-sensu-do-brasil) e os de 2013 a 2016, [desse](https://dadosabertos.capes.gov.br/dataset/discentes-da-pos-graduacao-stricto-sensu-do-brasil).

Os arquivos foram renomeados para facilitar a análise. A convenção de nomes é `ANO_REFERENCIA-ANO_PUB-MES_PUB-DIA_PUB.csv`. Os dados baixados da CAPES usam codificação ISO-8859-1. É necessário convertê-los para UTF-8 usando o comando iconv. Como os arquivos também são muito grandes é uma boa idéia comprimí-los.

```
for i in 20*.csv; do
  iconv -f ISO-8859-1 -t UTF-8 "$i" | zstd -o "$i.zst"
  rm "$i"
done
```

Para análise, será considerado apenas o PPGCC, é mais eficiente filtrar o PPGCC no terminal do que carregar todos os PPGs do brasil e filtrar no R. Para fazer isso:

```
for i in *.csv.zst; do
  SUB="ppgcc-$(echo $i | sed 's/.csv.zst/.csv/')"
  zstdcat "$i" | head -n 1 > $SUB
  zstdcat "$i" | grep 41001010025 >> $SUB
  echo "$SUB: $(($(wc -l <"$SUB") - 1)) registros"
done
```

Para obter defesas após a atualização do sucupira, use o [console web](https://developers.google.com/calendar/v3/reference/events/list?apix_params=%7B%22calendarId%22%3A%22ppgccnuvem%40gmail.com%22%7D) para baixar o calenario do PPGCC como json e salve o json no arquivo `calendar.json`. Para gerar o calendar.csv execute o script `calendar.py`
