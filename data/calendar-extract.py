import json
import re

rx_summary = re.compile(r'(?i)Defesa *(?:de)? *(Mestrado|Doutorado) *[-:]? *(.*)')
rx_descr = re.compile(r'(?i)ORIENTADORA?: *(?:prof.?\.?)? *(?:dr.?\.)? *((?:\w| )+)')
rx_coorientador_err = re.compile(r'COORIENTADOR:? *$')
rx_coorientador = re.compile(r'(?i)COORIENTADORA?: *(?:prof.?\.?)? *(?:dr.?\.)? *((?:\w| )+)')
rx_dt = re.compile(r'^([0-9]+-[0-9]+-[0-9]+)')
rx_sp = re.compile(r'  +')

def cleanup_string(s):
    return rx_sp.sub(' ', s.strip())

j = json.load(open('calendar.json', 'r'))
out = open('calendar.csv', 'w')
out.write('Curso,Discente,Orientador,Coorientador,Defesa\n')
for it in j['items']:
    match = rx_summary.search(it['summary'])
    if match != None:
        curso    = cleanup_string(match.group(1).upper())
        discente = cleanup_string(match.group(2).upper())
        match = rx_descr.search(it['description'])
        if match == None:
            print(f'Falha ao estrair orientador para Defesa de {curso} - ' +
                  f'{discente} da descrição')
        else:
            orientador = cleanup_string(match.group(1).upper())
            orientador = rx_coorientador_err.sub('', orientador)
            coorientador = ''
            matcher = rx_coorientador.search(it['description'])
            if matcher != None:
                coorientador = cleanup_string(matcher.group(1).upper())
            start = it['start']['dateTime']
            matcher = rx_dt.search(start)
            start = start if matcher == None else matcher.group(1)
            out.write(f'{curso},{discente},{orientador},{coorientador},{start}\n')
out.flush()
out.close()
