---
title: "Formação"
output: 
  html_document: 
    toc: yes
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE);

library(ggplot2);
library(lubridate);
library(stringi);

theme_set(theme_bw())

parse_dt_br <- function(dt.str) {
  rx <- '(?i)^ *([0-9]+)[ -]*([a-z]+)[ -]*([0-9][0-9]+).*';
  iso <- paste(gsub(rx, '\\3', dt.str), gsub(rx, '\\2', dt.str), gsub(rx, '\\1', dt.str), sep='-');
  iso <- gsub('^([0-9][0-9]-.*)$', '20\\1', iso)
  en <- c('jan', 'feb', 'mar', 'apr', 'may', 'jun', 'jul', 'aug', 'sep', 'oct', 'nov', 'dec');
  pt <- c('jan', 'fev', 'mar', 'abr', 'mai', 'jun', 'jul', 'ago', 'set', 'out', 'nov', 'dez');
  for (i in 1:12) {
    iso <- gsub(paste('(?i)',en[i],'[^-]*',sep=''), sprintf('%02d', i), iso);
    iso <- gsub(paste('(?i)',pt[i],'[^-]*',sep=''), sprintf('%02d', i), iso);
  }
  return(ymd(iso));
}
months_diff <- function(start, end) {
  return(round((as.double(difftime(end, start))/365)*12));
}
as_semestre <- function(d) {
  sprintf('%d/%d', year(d), ifelse(month(d) <= 6, 1, 2)) 
}
```

## Carregar


```{r load}
columns_wanted <- c('AN_BASE', 'ID_PESSOA', 'NM_DISCENTE','DS_GRAU_ACADEMICO_DISCENTE',
                    'NM_SITUACAO_DISCENTE', 'DT_MATRICULA_DISCENTE', 
                    'DT_SITUACAO_DISCENTE', 'QT_MES_TITULACAO', 
                    'NM_ORIENTADOR')
columns_wanted2 <- sub('NM_ORIENTADOR', 'NM_ORIENTADOR_PRINCIPAL', columns_wanted)
columns_renamed <- c('Base', 'Discente.id', 'Discente','Curso', 'Situacao', 'Matricula',
                     'Situacao.dt', 'Meses',  'Orientador')
add_csv <- function(df, path, projection) {
  tmp <- read.csv(path, sep=';')[,projection]
  colnames(tmp) <- columns_renamed
  return(rbind(df, tmp));
}

df <- data.frame()
df <- add_csv(df, 'data/ppgcc-2013-2017-12-02.csv', columns_wanted)
df <- add_csv(df, 'data/ppgcc-2014-2017-12-02.csv', columns_wanted)
df <- add_csv(df, 'data/ppgcc-2015-2017-12-02.csv', columns_wanted)
df <- add_csv(df, 'data/ppgcc-2016-2017-12-02.csv', columns_wanted)
df <- add_csv(df, 'data/ppgcc-2017-2018-07-01.csv', columns_wanted2)
df <- add_csv(df, 'data/ppgcc-2018-2019-10-01.csv', columns_wanted2)
df <- df[!duplicated(df[,c('Discente.id', 'Curso')], fromLast=T),]

# Trata datas
df$Matricula   <- as.Date(sapply(df$Matricula, parse_dt_br), origin="1970-01-01");
df$Situacao.dt <- as.Date(sapply(df$Situacao.dt, parse_dt_br), origin="1970-01-01");
df$Meses <- ifelse(df$Meses>0, df$Meses, 
                   sapply(1:nrow(df), function(i){months_diff(df$Matricula[i], df$Situacao.dt[i])}))
df$Meses <- ifelse(df$Situacao!='MATRICULADO', df$Meses, 
                   sapply(1:nrow(df), function(i){months_diff(df$Matricula[i], now())}))
df_compute_semestre <- function(df) {
  df$Semestre.curso <- 1 + floor(df$Meses / 6);
  df$Semestre <- sapply(df$Situacao.dt, as_semestre)
  df$Semestre <- factor(df$Semestre, levels = sort(unique(df$Semestre)))
  df$Matricula.Semestre <- sapply(df$Matricula, as_semestre)
  df$Matricula.Semestre <- factor(df$Matricula.Semestre, levels = sort(unique(df$Matricula.Semestre)))
  return(df);
}
df <- df_compute_semestre(df);

# Remove acentos (já vem sem, apenas garante uniformidade)
fix_nomes <- function(df) {
  if ('Discente' %in% colnames(df)) {
    df$Discente <- stri_trans_general(str = df$Discente, id = "Latin-ASCII");
    df$Discente <- sub('LUCAS VIANA KNOCHENHUAER', 'LUCAS VIANA KNOCHENHAUER', df$Discente);
  }
  df$Orientador <- stri_trans_general(str = df$Orientador, id = "Latin-ASCII")
  df$Orientador <- sub('ANTONIO AUGUSTO MEDEIROS FROHLICH', 'ANTONIO AUGUSTO FROHLICH', df$Orientador);
  df$Orientador <- sub('CHRISTIANE ANNELIESE GRESSE VON WANGENHEIM', 'CHRISTIANE GRESSE VON WANGENHEIM', df$Orientador);
  df$Orientador <- gsub('JOSE LUIS A$', 'JOSE LUIS ALMADA GUNTZEL', df$Orientador);
  df$Orientador <- sub('MARCIO BASTOS CASTRO', 'MARCIO CASTRO', df$Orientador);
  return(df);
}
df <- fix_nomes(df);

# Cria um status INGRESSO, duplicando todos os alunos.
df.ingresso <- data.frame(Discente.id=df$Discente.id, Curso=df$Curso, Situacao='INGRESSO', 
                          Situacao.dt=df$Matricula, Semestre=df$Matricula.Semestre)
df.ingresso <- rbind(df.ingresso, df[df$Situacao!='MATRICULADO',colnames(df.ingresso)])
```

Os dados até então carregados dizem respeito apenas aos dados baixados da CAPES. Para atualizar com as ocorrências de 2019, foram baixadas as defesas do calendário do PPGCC. Será mostrada a lista de alunos que defenderam mas não estavam no sucupira.
```{r calendar}
df.cal <- read.csv('data/calendar.csv', na.strings = '')
df.cal$Defesa <- as.Date(df.cal$Defesa)
df.cal$TemCoorientador <- is.na(df.cal$Coorientador)
df.cal$TemCoorientador.text <- ifelse(df.cal$TemCoorientador, 'Tem Coorientador', 'Não Tem Coorientador');
df.cal <- fix_nomes(df.cal);

# Mostra alunos que defenderam mas não estavam no Sucupira
df.cal[!(df.cal$Discente %in% df$Discente),]

# Preenche df.cal com dados em df
df.cal <- merge(df.cal, df[,setdiff(colnames(df), c('Orientador'))], 
           by=c('Curso', 'Discente'));
df.cal$Meses <- ifelse(df.cal$Situacao=='TITULADO', df.cal$Meses, 
                    sapply(1:nrow(df.cal), function(i){months_diff(df.cal$Matricula[i], df.cal$Defesa[i])}));

# Seleciona apenas alunos não TITULADOs no Sucupira e os atualiza em df
x <- df.cal[df.cal$Situacao=='MATRICULADO', setdiff(colnames(df.cal), c('Situacao.dt', 'Coorientador', 'TemCoorientador', 'TemCoorientador.text'))]
x$Situacao <- 'TITULADO'; # falhará se executado mais de uma vez!
colnames(x) <- sub('Defesa', 'Situacao.dt', colnames(x));
x <- df_compute_semestre(x)
x$Base <- as.integer(2019);
df <- rbind(df, x)
df <- df[!duplicated(df[,c('Discente.id', 'Curso')], fromLast=T),]

#Prepar df.cal para uso posterior
df.cal <- df.cal[,setdiff(colnames(df.cal), c('Situacao.dt'))];
df.cal$Situacao <- 'TITULADO';
df.cal$Meses <- sapply(1:nrow(df.cal), function(i){months_diff(df.cal$Matricula[i], df.cal$Defesa[i])});
colnames(df.cal) <- sub('Defesa', 'Situacao.dt', colnames(df.cal));
df.cal <- df_compute_semestre(df.cal)
```

Carrega Linhas de pesquisa e pontos (após ter completado df)
```{r load_linhas}
# Carrega linhas
df.linhas <- read.csv('data/linhas.csv');
df$Linha <- sapply(df$Orientador, function(prof){df.linhas$Linha[df.linhas$Orientador==prof]})

# Pontos
df.pontos <- read.csv('data/pontos.csv');
df.pontos$Orientador <- toupper(df.pontos$Orientador);
df.pontos <- fix_nomes(df.pontos)
df$Pontos <- sapply(df$Orientador, function(o){df.pontos$Pontos[df.pontos$Orientador==o][1]})
```

## Situação geral

Alunos por status
```{r status_count}
ggplot(df, aes(x=Situacao, fill=Curso)) +
  geom_bar(position=position_dodge()) +
  scale_fill_brewer(palette = 'Set1') +
  labs(x='Última Situação', y='Número de alunos')
```

"Idade", em semestres, dos alunos dentro do curso.
```{r}
f <- function(df, scales=NULL) {
  tmp <- expand.grid(x=sort(unique(df$Semestre.curso)), 
                     Curso=unique(df$Curso), 
                     Situacao=unique(df$Situacao),
                     y=0);
  tmp$y = sapply(1:nrow(tmp), function(i){
    nrow(df[df$Semestre.curso==tmp$x[i] & df$Curso==tmp$Curso[i] & df$Situacao==tmp$Situacao[i],])
  });
  ggplot(tmp, aes(x, y, fill=Curso)) +
    facet_wrap(~Situacao, ncol=2, scales=scales) +
    geom_bar(stat='identity', position=position_dodge()) +
    scale_x_continuous(breaks=1:max(df$Semestre.curso)) +
    scale_fill_brewer(palette = 'Set1') +
    theme(legend.position = 'bottom') +
    labs(x='Semestre', y='Número de alunos')
}
f(df[df$Situacao %in% c('DESLIGADO', 'ABANDONOU'),])
f(df[df$Situacao %in% c('TITULADO', 'MATRICULADO'),], 'free_y')
```

Mapa de calor para o ingresso/saída.
```{r hm.ingresso}
f <- function(df, title) {
  ggplot(df, aes(x=Semestre, y=Situacao)) + 
    theme(axis.text.x = element_text(angle=90, vjust=0.5)) +
    geom_bin2d() +
    #scale_fill_gradient(low = '#542788', high='#b35806') +
    labs(y='Evento', fill='Discentes', title = title)
}

f(df.ingresso[df.ingresso$Curso=='MESTRADO',], 'Mestrado');
f(df.ingresso[df.ingresso$Curso=='DOUTORADO',], 'Doutorado');
f(df.ingresso, 'Geral');
```

## Cruzamentos

Coorientador reduz o prazo? Dataset: Defesas de 2018 e 2019, extraídas do calendário do PPGCC
```{r coorient}
ggplot(df.cal, aes(x=Semestre.curso, fill=Curso)) +
  facet_wrap(~ TemCoorientador.text) +
  geom_bar(position=position_dodge()) +
  scale_x_continuous(breaks=1:max(df$Semestre.curso)) +
  scale_fill_brewer(palette = 'Set1') +
  labs(x='Semestre da defesa', y='Número de alunos')

ggplot(df.cal, aes(x=Curso, y=Meses, fill=ifelse(TemCoorientador, 'Sim', 'Não'))) +
  geom_violin() +
  scale_fill_brewer(palette = 'Set1') +
  labs(x='Curso', y='Mês da defesa', fill='Tem Coorientador')
```


Orientador vs. Tempo até titulação. Muitos poucos dados no doutorado.
```{r Orientador, fig.height=6}
orientadoresAtivos <- unique(df$Orientador[df$Base>2016])
tmp <- df[df$Situacao=='TITULADO' & df$Orientador %in% orientadoresAtivos,]
f <- function(curso) {
  x <- data.frame(tmp[tmp$Curso==curso,])
  x <- x[order(sapply(x$Orientador, function(o){mean(df$Meses[df$Curso==curso & df$Orientador==o])})),]
  x$Orientador <- factor(x$Orientador, levels=unique(x$Orientador))
  ggplot(x, aes(x=Semestre.curso, y=Orientador)) +
    scale_x_continuous(breaks = 1:max(tmp$Semestre.curso)) +
    geom_bin2d() +
    labs(x='Semestre de conclusão', y='Orientador', fill='Número de alunos', title=stri_trans_totitle(curso))
}
f('MESTRADO')
f('DOUTORADO')
```

Linha de pesquisa do Orientador vs. Tempo até titulação. Para algumas combinações há no máximo um aluno, o que impede a renderização do violin de doutorado (o de mestrado fica entre os pontos).
```{r linha}
orientadoresAtivos <- unique(df$Orientador[df$Base>2016])
tmp <- df[df$Situacao=='TITULADO' & df$Orientador %in% orientadoresAtivos,]
ggplot(tmp, aes(x=Linha, y=Meses, fill=Curso)) +
  geom_violin(position=position_dodge(.9)) +
  geom_point(aes(color=Curso), position=position_dodge(.9), shape=1, show.legend=F) +
  scale_fill_brewer(palette = 'Set1') +
  theme(legend.position = 'bottom', axis.text.x = element_text(angle=45, vjust=1,  hjust=1))
```

Pontos no quadriênio atual (2019-10-20) vs. Tempo até titulação
```{r pontos}
tmp <- df[df$Situacao=='TITULADO' & df$Orientador %in% orientadoresAtivos,]

ggplot(tmp, aes(x=Pontos,y=Meses,color=Curso)) +
  facet_wrap(~Linha) +
  scale_fill_brewer(palette = 'Set1') +
  geom_point(na.rm=T) +
  labs(y='Meses para Conclusão', x='Pontos SICLAP (2019-10-20)')

ggplot(tmp, aes(x=Pontos,y=Meses,color=Linha)) +
  facet_wrap(~Curso) +
  scale_fill_brewer(palette = 'Set1') +
  geom_point(na.rm=T) +
  labs(y='Meses para Conclusão', x='Pontos SICLAP (2019-10-20)')
```